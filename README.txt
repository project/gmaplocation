**Description:

The gmaplocation module allow displaying of one geographic location via Google Maps. It has following features:

* show Google Maps with marker on one location
* editor can drag marker to fine-tune position in case geolocating did not found accurate address
* creates a block with static image of map and link to gmaplocationpage

**Credits:

Bojan Mihelac (bojan AT informatikamihelac.com)
http://source.mihelac.org
