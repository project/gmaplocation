$(document).ready(function() {
	// when address change, update remove lat and lng
	$("#edit-gmaplocation-address").bind('change', function() {
		$('#edit-gmaplocation-lat').val("");
		$('#edit-gmaplocation-lng').val("");
	});
});